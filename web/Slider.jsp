<%@page import="Beans.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Slider</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link rel="stylesheet" href="assets/css/slider.css">
        <script src="assets/jquery-2.1.4.min.js"></script>
    </head>
    <%
        ArrayList<Usuario> user= (ArrayList<Usuario>) session.getAttribute("user");
        if(user!=null){
        %> 
    <body>
        <div id="c-slider">
            <div id="slider">
                <section><img src="assets/imagenes/01.jpg" width="700px" height="300px"  alt=""></section>
                <section><img src="assets/imagenes/02.jpg" width="700px" height="300px" alt=""></section>
                <section><img src="assets/imagenes/03.jpg" width="700px" height="300px" alt=""></section>
                <section><img src="assets/imagenes/04.jpg" width="700px" height="300px" alt=""></section>
                <section><img src="assets/imagenes/05.jpg" width="700px" height="300px" alt=""></section>
                <section><img src="assets/imagenes/06.jpg" width="700px" height="300px" alt=""></section>
            </div>
            <div id="btn-prev">&#60</div>
            <div id="btn-next">&#62</div>
        </div>

        <script src="assets/js/slider.js"></script>
    </body>
    <%}else{
            response.sendRedirect("index.jsp");
        }%> 
</html>
