<%@page import="Beans.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Beans.Almacen"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="sAlmacen?opcion=listarAlmacen" />
<jsp:useBean id="alma" scope="request" class="java.util.ArrayList"/>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Almacen</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <script src="assets/js/main.js"></script>
    </head>
     <%
        ArrayList<Usuario> user= (ArrayList<Usuario>) session.getAttribute("user");
        if(user!=null){
        %> 
    <body>
        <jsp:include  page="Header.jsp"></jsp:include>
            <br>
            <br>
            <aside class="col-md-2 " style="border-right:1px solid #ccc;height:500px"> 

            <jsp:include  page="MenuVertical.jsp"></jsp:include>
            </aside>
            <section class="posts col-md-10">
                <div class="container">
                    <p><br></p>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-9">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="page-header text-center">
                                        <h1>Registro de Almacen</h1>
                                    </div>
                                    <table class="table table-bordered col-md-9">
                                        <tbody>
                                            <tr>
                                                <td align="center">   Id   </td>
                                                <td align="center">     Producto      </td>
                                                <td align="center">     Categoria     </td>
                                                <td align="center">     Codigo       </td>
                                                <td align="center">     Fecha       </td>
                                                <td align="center">     Cantidad      </td>
                                            </tr>
                                            </tr>
                                        <%
                                            if (alma != null) {

                                                for (int i = 0; i < alma.size(); i++) {
                                                    Almacen pro = (Almacen) alma.get(i);
                                        %>

                                        <tr><td><% out.println(i+1); %></td>
                                            <td><% out.println(pro.getProducto().getDescripcion().toUpperCase()); %></td>
                                            <td><% out.println(pro.getProducto().getCateg().getNombre().toUpperCase()); %></td>
                                            <td><% out.println(pro.getProducto().getCodigo().toUpperCase()); %></td>
                                            <td><% out.println(pro.getFecha()); %></td>
                                            <td><% out.println(pro.getCantidad()); %></td>
                                            <%}
                                        }%>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
    <%}else{
            response.sendRedirect("index.jsp");
        }%> 
</html>
