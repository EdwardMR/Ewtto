<%@page import="Beans.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

        <link rel="stylesheet" href="assets/css/estilos.css">
        <link rel="stylesheet" href="assets/fonts.css">
        <link rel="stylesheet" href="assets/icons.css">
        <script src="assets/jquery-2.1.4.min.js"></script>
        <script src="assets/js/main.js"></script>

       
        <title>Menu Vertical</title>
    </head>
     <%
        ArrayList<Usuario> user= (ArrayList<Usuario>) session.getAttribute("user");
        if(user!=null){
        %> 
    <body>

        <ul class="nav nav-pills nav-stacked admin-menu">
            <li class="active btn-primary"><a href="MenuPrincipal.jsp" data-target><i class="icon-home"></i> Home</a></li>
            <%if(user.get(0).getTipo().equals("A") || user.get(0).getTipo().equals("V")){%>
            <li><a href="Clientes.jsp" data-target-id="widgets"><i></i>Clientes</a></li>
            <%}%>
            <%if(user.get(0).getTipo().equals("A")){%>
            <li><a href="Categorias.jsp" data-target-id="charts"><i class=""></i>Categorias</a></li>
            <%}%>
            <%if(user.get(0).getTipo().equals("A")){%>
            <li><a href="Productos.jsp" data-target-id="table"><i class=""></i>Productos</a></li>
            <%}%>
            <%if(user.get(0).getTipo().equals("A") || user.get(0).getTipo().equals("V")){%>
            <li><a href="Venta.jsp" data-target-id="calender"><i class=""></i>Ventas</a></li>
            <%}%>
            <%if(user.get(0).getTipo().equals("A") || user.get(0).getTipo().equals("V")){%>
            <li><a href="VentasDia.jsp" data-target-id="calender"><i class=""></i>Listar Ventas del Dia</a></li>
            <%}%>
            <%if(user.get(0).getTipo().equals("A") || user.get(0).getTipo().equals("C")){%>
            <li><a href="Almacen.jsp" data-target-id="calender"><i class=""></i>Registrar Almacen</a></li>
            <%}%>
             <%if(user.get(0).getTipo().equals("A") || user.get(0).getTipo().equals("C")){%>
            <li><a href="ActualizarStock.jsp" data-target-id="calender"><i class=""></i>Actualizar Stock</a></li>
            <%}%>
             <%if(user.get(0).getTipo().equals("A") || user.get(0).getTipo().equals("C")){%>
            <li><a href="ListaAlmacen.jsp" data-target-id="calender"><i class=""></i>Listar Almacen</a></li>
            <%}%>
            <%if(user.get(0).getTipo().equals("A")){%>
            <li><a href="Usuarios.jsp" data-target-id="applications"><i class=""></i>Usuarios</a></li>
            <%}%>
        </ul>
        <br>
            <div class="col-md-3"></div>
            <div class="panel panel-primary text-center">
                <div class="panel-heading">
                    <h6 class=""><span class=""></span>VENTA DE RELOJES, CALCULADORAS, LENTES<span class=""></span></h6>
                    <h6 class=""><span class=""></span>JUGUETERÍA EN GENERAL Y REGALOS PARA TODA OCASIÓN<span class=""></span></h6>
                    <h6 class=""><span class=""></span>AL POR MAYOR Y MENOR<span class=""></span></h6>
                </div>
            </div>
        <!-- jQuery -->
        <script src="assets/jquery-2.1.4.min.js"></script>
        <!--<script src="//code.jquery.com/jquery.js"></script>-->
        <!-- Bootstrap JavaScript -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    </body>
     <%}else{
            response.sendRedirect("index.jsp");
        }%> 
</html>
