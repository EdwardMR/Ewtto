<%@page import="Beans.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Modificar Cliente</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    </head>
    <%
        ArrayList<Usuario> user= (ArrayList<Usuario>) session.getAttribute("user");
        if(user!=null){
        %> 
    <body>
        <jsp:include  page="Header.jsp"></jsp:include>
            <br>
            <br>
            <aside class="col-md-2 " style="border-right:1px solid #ccc;height:500px"> 
            <jsp:include  page="MenuVertical.jsp"></jsp:include>
            </aside>


            <section  class="posts col-md-10" style="background: lightgrey">
                <br>

                <div class="form-group col-md-15" style="border-left:1px solid #ccc;height:200px">
                    <!--<div class="container-fluid" -->
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="panel-heading text-center">
                                        <h1>Modificar Cliente</h1>
                                    </div>
                                    <div>
                                        <form name="sCliente" id="sCliente" action="sCliente" method="POST">
                                           <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">Id</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                                                    <input type="text" class="form-control" name="id" id="id" value="<%out.println(request.getParameter("cli_id"));%>" placeholder="id" aria-describedby="basic-addon1" required="required" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Nombre</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                                                <input type="text" class="form-control" name="nombre" id="nombre" value="<%out.println(request.getParameter("cli_nom")); %>" placeholder="Nombre" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Apellido Paterno</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                                                <input type="text" class="form-control" name="apaterno" id="apaterno" value="<%out.println(request.getParameter("cli_apat"));%>" placeholder="Apellido Paterno" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Apellido Materno</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                                                <input type="text" class="form-control" name="amaterno" id="amaterno" value="<%out.println(request.getParameter("cli_amat"));%>" placeholder="Apellido Materno" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Direccion</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-home"></span></span>
                                                <input type="text" class="form-control" name="direccion" id="direccion" value="<%out.println(request.getParameter("cli_dir"));%>" placeholder="Direccion" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">telefono</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-phone"></span></span>
                                                <input type="text" class="form-control" name="telefono" id="telefono" value="<%out.println(request.getParameter("cli_tel"));%>" placeholder="Telefono" maxlength="9" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">DNI</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-credit-card"></span></span>
                                                <input type="text" class="form-control" name="dni" id="dni" value="<%out.println(request.getParameter("cli_dni"));%>" placeholder="DNI" maxlength="8" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                            <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">RUC</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-credit-card"></span></span>
                                                <input type="text" class="form-control" name="Ruc" id="RUC" value="<%out.println(request.getParameter("cli_Ruc"));%>" placeholder="RUC" aria-describedby="basic-addon1">
                                            </div>
                                        </div>
                                        <div class="text-center form-group col-md-12" >
                                            <button type="submit" class="btn btn-danger" value="Modificar" name="opcion" id="opcion"><span class="glyphicon glyphicon-remove"></span> Modificar </button>
                                            <button type="submit" class="btn btn-primary" value="Regresar" name="opcion" id="opcion"><span class="glyphicon glyphicon-pencil"></span> Regresar </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


        </section>

        <!-- jQuery -->
        <script src="assets/jquery-2.1.4.min.js"></script>
        <!--<script src="//code.jquery.com/jquery.js"></script>-->
        <!-- Bootstrap JavaScript -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    </body>
     <%}else{
            response.sendRedirect("index.jsp");
        }%> 
</html>