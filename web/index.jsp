<%-- 
    Document   : index
    Created on : 01-abr-2016, 19:16:53
    Author     : JORGE UCHOFEN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <title>Login</title>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <!-- Bootstrap -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            html, body {
                height: 100%;
                width: 100%;
                padding: 0;
                margin: 0;
            }

            #full-screen-background-image {
                z-index: -999;
                width: 100%;
                height: auto;
                position: fixed;
                top: 0;
                left: 0;
            }
        </style>

    </head>
    <img alt="full screen background image" src="assets/imagenes/fondo.jpg" id="full-screen-background-image" /> 
    <div class="panel panel-primary text-center">
        <div class="panel-heading">
            <h1><img src="assets/imagenes/Logo.jpg" />  <span class="glyphicon glyphicon-align-center"></span><FONT FACE="impact" SIZE=7 COLOR="white">  EMPRESA EWTTO </FONT>  <span class="glyphicon glyphicon-align-center"></span>  <img src="assets/imagenes/Logo.jpg" /></h1>
            <br>
        </div>
    </div>
    <br>
    <br>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="text-center">
                        <H1>Acceso al Sistema</H1>
                    </div>
                    <div class="panel-body">

                        <form name="sLogin" id="sLogin" action="sLogin" method="POST">
                            <div class="form-group col-md-5">
                                <div class="slider">

                                    <a><img src="assets/imagenes/login.png" width="200px" height="200"  ></a>


                                </div>
                            </div>
                            <div class="form-group col-md-7" style="border-left:1px solid #ccc;height:200px">

                                <div>
                                    <label>Usuario</label>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                                        <input type="text" name="usuario" id="usuario" class="form-control" placeholder="Usuario" aria-describedby="basic-addon1" >
                                    </div>
                                </div>
                                <p><br></p>
                                <div>
                                    <label>Contraseña</label>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-lock"></span></span>
                                        <input type="password" name="clave" id="clave" class="form-control" placeholder="Contraseña" aria-describedby="basic-addon1">
                                    </div>
                                </div>
                                <p><br></p>
                                <div class="form-group col-md-12">
                                    <div class="text-center"> 
                                        <button type="submit" value="Ingresar" name="opcion" id="opcion" class="btn btn-success" ><span class="glyphicon glyphicon-ok"></span> Ingresar </button>

                                        <button type="submit" value="Cancelar" name="opcion" id="opcion" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</body>
</html>

