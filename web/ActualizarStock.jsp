<%@page import="Beans.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Beans.Producto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="sAlmacen?opcion=listarProducto" />
<jsp:useBean id="listapro" scope="request" class="java.util.ArrayList"/>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Almacen</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <script src="assets/js/main.js"></script>
        <script src="assets/js/BuscarTabla.js"></script>
    </head>
    <%
        ArrayList<Usuario> user = (ArrayList<Usuario>) session.getAttribute("user");
        if (user != null) {
    %> 
    <body>
        <jsp:include  page="Header.jsp"></jsp:include>
            <br>
            <br>
            <aside class="col-md-2 " style="border-right:1px solid #ccc;height:500px"> 

            <jsp:include  page="MenuVertical.jsp"></jsp:include>
            </aside>
            <section class="posts col-md-10">
                <div class="container">
                    <p><br></p>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-9">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="page-header text-center">
                                        <h1>Actualizar Stock</h1>
                                    </div>
                                    <form name="sAlmacen" id="sAlmacen" action="sAlmacen" method="POST">
                                        <div class="text-center" >
                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#agregar">
                                                Agregar Producto
                                            </button>                                        
                                        </div>
                                    </form>
                                    <br>
                                    <form name="sAlmacen" id="sAlmacen" action="sAlmacen" method="get">
                                        <table class="table table-bordered col-md-9">
                                            <tbody>
                                                <tr>
                                                    <td align="center">   Id   </td>
                                                    <td align="center">     Codigo      </td>
                                                    <td align="center">     Nombre      </td>
                                                    <td align="center">     Stock      </td>
                                                    <td align="center">     Cantidad      </td>
                                                    <td align="center">    </td>
                                                </tr>                  
                                            <%
                                                ArrayList<Producto> lProd = (ArrayList<Producto>) session.getAttribute("carrito");
                                                if (lProd != null) {
                                                    for (int i = 0; i < lProd.size(); i++) {
                                                        Producto pro = (Producto) lProd.get(i);
                                            %>
                                            <tr><td><% out.println(i+1); %></td>
                                                <td><% out.println(pro.getCodigo()); %></td>
                                                <td><% out.println(pro.getDescripcion().toUpperCase()); %></td>
                                                <td><% out.println(pro.getStock()); %></td>
                                                <td><input type="text" class="form-control" name="almacen<%out.print(i);%>" id="almacen" placeholder="Actualizar el Stock" aria-describedby="basic-addon1" required="required"></td>
                                                <td align="center">
                                                    <a href="sAlmacen?opcion=RemoverCarrito1&pro_id=<%out.println(pro.getId());%>"><span class="glyphicon glyphicon-trash"></span></a>
                                                </td>
                                                <%}
                                                    }%>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="text-center" >
                                        <button type="submit" class="btn btn-primary" value="Actualizar" name="opcion" id="opcion"><span class="glyphicon glyphicon-pencil"></span> Actualizar Stock </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <div class="modal fade" id="agregar" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Listado de Productos</h4>
                    </div>
                    <br>
                    <div class="box">
                        <center>
                            <div class="box-header" id="buscar">Buscar <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro"></div>
                        </center>
                    </div>
                        <br>
                        <div class="modal-body">
                            <table class="order-table table">
                                <tbody>
                                    <tr>
                                        <td align="center">   Id   </td>
                                        <td align="center">     Codigo      </td>
                                        <td align="center">     Nombre      </td>
                                        <td align="center">     Marca       </td>
                                        <td align="center">     Stock      </td>
                                        <td align="center">    </td>
                                    </tr>
                                    <%
                                        if (listapro != null) {

                                            for (int i = 0; i < listapro.size(); i++) {
                                                Producto pro = (Producto) listapro.get(i);
                                    %>

                                    <tr><td><% out.println(pro.getId()); %></td>
                                        <td><% out.println(pro.getCodigo()); %></td>
                                        <td><% out.println(pro.getDescripcion().toUpperCase()); %></td>
                                        <td><% out.println(pro.getMarca().toUpperCase()); %></td>
                                        <td><% out.println(pro.getStock()); %></td>
                                        <td align="center">
                                            <a href="sAlmacen?opcion=Carrito1&prod_id=<%out.println(pro.getId());%>"><span class="glyphicon glyphicon-plus"></span></a>
                                        </td>
                                        <%}
                                        }%>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </body>
    <%} else {
            response.sendRedirect("index.jsp");
        }%> 
</html>
