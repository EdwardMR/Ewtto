<%@page import="Beans.Compras"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Beans.Usuario"%>
<%@page import="java.util.ArrayList"%>
<jsp:include page="sReportes?opcion=listacompra" />
<jsp:useBean id="listapro" scope="request" class="java.util.ArrayList"/>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ventas</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <script src="assets/js/main.js"></script>
        <script src="assets/js/BuscarTabla.js"></script>
    </head>
    <%
        ArrayList<Usuario> user = (ArrayList<Usuario>) session.getAttribute("user");
        if (user != null) {
    %> 
    <body>
        <jsp:include  page="Header.jsp"></jsp:include>
            <br>
            <br>
            <aside class="col-md-2 " style="border-right:1px solid #ccc;height:500px"> 

            <jsp:include  page="MenuVertical.jsp"></jsp:include>
            </aside>
            <section class="posts col-md-10">
                <div id="listado" class="tab-pane fade in active">
                    <div class="box">
                        <div class="box-header" align="center">
                            <h3 class="box-title">Lista de Ventas</h3>
                        </div><!-- /.box-header -->
                        <br>
                        <div class="box">
                            <center>
                                <div class="box-header" id="buscar">Buscar <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro"></div>
                            </center>
                        </div>
                        <div class="box-body">
                            <br>
                            <table class="order-table table">
                                <thead>
                                    <tr>
                                        <td align="center">Id</td>
                                        <td align="center">Fecha</td>
                                        <td align="center">Documento</td>
                                        <td align="center">Numero</td>
                                        <td align="center">VerDetalle</td>
                                    </tr>  
                                </thead>
                                <tbody>
                                <%
                                    if (listapro != null) {
                                        for (int i = 0; i < listapro.size(); i++) {
                                            Compras pro = (Compras) listapro.get(i);
                                %>
                                <tr><td align="center"><% out.println(i + 1); %></td>
                                    <td align="center"><% out.println(pro.getFecha()); %></td>
                                    <td align="center"><%if (pro.getDocumento().equals("F")) {out.print("FACTURA");}else{out.print("BOLETA");} %></td>
                                    <td align="center"><% out.println(pro.getNumero()); %></td>
                                    <td align="center">
                                        <a href="DetalleCompra.jsp?ven_id=<%out.println(pro.getId());%>" target="_blank"><button class="btn btn-block btn-info btn-sm">VerDetalle</button></a>
                                    </td>

                                    <%}
                                            }%>
                                </tr>
                            </tbody>
                        </table>
                    </div><!-- /.box-body --> 
                </div>    
            </div>
        </section>
    </body>
    <%} else {
            response.sendRedirect("index.jsp");
        }%> 
</html>
