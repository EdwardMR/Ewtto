<%@page import="Beans.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Menu Principal</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <script src="assets/js/main.js"></script>

    </head>
    <%
        ArrayList<Usuario> user= (ArrayList<Usuario>) session.getAttribute("user");
        if(user!=null){
        %> 
    <body>
         <jsp:include  page="Header.jsp"></jsp:include>
         <br>
         <br>
                <aside class="col-md-2 " style="border-right:1px solid #ccc;height:500px"> 
                <jsp:include  page="MenuVertical.jsp"></jsp:include>
                </aside>
         <section class="posts col-md-10">
         <jsp:include  page="Slider.jsp"></jsp:include>
         </section>
        <!-- jQuery -->
        <script src="assets/jquery-2.1.4.min.js"></script>
        <!--<script src="//code.jquery.com/jquery.js"></script>-->
        <!-- Bootstrap JavaScript -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    </body>
    <%}else{
            response.sendRedirect("index.jsp");
        }%> 
</html>
