<%@page import="Beans.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Beans.Categoria" %>
<jsp:include page="sCategorias?opcion=LlenarCombobox" />
<jsp:useBean id="lista" scope="request" class="java.util.ArrayList"/>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Modificar Producto</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    </head>
    <%
        ArrayList<Usuario> user = (ArrayList<Usuario>) session.getAttribute("user");
        if (user != null) {
    %> 
    <body>
        <jsp:include  page="Header.jsp"></jsp:include>
            <br>
            <br>
            <aside class="col-md-2 " style="border-right:1px solid #ccc;height:500px"> 
            <jsp:include  page="MenuVertical.jsp"></jsp:include>
            </aside>


            <section  class="posts col-md-10" style="background: lightgrey">
                <br>

                <div class="form-group col-md-15" style="border-left:1px solid #ccc;height:200px">
                    <!--<div class="container-fluid" -->
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="panel-heading text-center">
                                        <h1>Modificar Producto</h1>
                                    </div>
                                    <div>
                                        <form name="sProducto" id="sProducto" action="sProducto" method="POST">
                                            <div class="form-group col-md-6">
                                                <label for="exampleInputEmail1">Id</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-th-large"></span></span>
                                                    <input type="text" class="form-control" name="id" id="id" value="<%out.println(request.getParameter("pro_id")); %>" placeholder="Codigo" aria-describedby="basic-addon1" required="required" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Codigo</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-th-large"></span></span>
                                                <input type="text" class="form-control" name="codigo" id="codigo" value="<%out.println(request.getParameter("pro_cod")); %>" placeholder="Codigo" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Descripcion</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-th-large"></span></span>
                                                <input type="text" class="form-control" name="nombre" id="nombre" value="<%out.println(request.getParameter("pro_des")); %>" placeholder="Descripcion" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Marca</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-th-large"></span></span>
                                                <input type="text" class="form-control" name="marca" id="marca" value="<%out.println(request.getParameter("pro_mar")); %>" placeholder="Marca" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Precio por Menor</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-euro"></span></span>
                                                <input type="text" class="form-control" name="preciomenor" id="preciomenor" value="<%out.println(request.getParameter("pro_pmn")); %>" placeholder="Precio por Menor" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Precio por Mayor</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-euro"></span></span>
                                                <input type="text" class="form-control" name="preciomayor" id="preciomayor" value="<%out.println(request.getParameter("pro_pmy")); %>" placeholder="Precio por Mayor" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Precio por Caja</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-euro"></span></span>
                                                <input type="text" class="form-control" name="preciocaja" id="preciocaja" value="<%out.println(request.getParameter("pro_caj")); %>" placeholder="Precio por Caja" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Precio por Factura</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-euro"></span></span>
                                                <input type="text" class="form-control" name="preciofactura" id="preciofactura" value="<%out.println(request.getParameter("pro_fact")); %>" placeholder="Precio por Factura" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Precio por Costo</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-euro"></span></span>
                                                <input type="text" class="form-control" name="preciocosto" id="preciocosto" value="<%out.println(request.getParameter("pro_cos")); %>" placeholder="Precio por Costo" aria-describedby="basic-addon1" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Stock</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-minus"></span></span>
                                                <input type="text" class="form-control" name="stock" id="stock" value="<%out.println(request.getParameter("pro_stk")); %>" placeholder="Stock" aria-describedby="basic-addon1">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Imagen</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-file"></span></span>
                                                <input type="file" class="form-inline" name="imagen" id="imagen" aria-describedby="basic-addon1">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Categoria</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-list"></span></span>
                                                <select class="form-control" name="categoria" id="categoria" aria-describedby="basic-addon1">
                                                        <%
                                                            for (int i = 0; i < lista.size(); i++) {
                                                                Categoria cat = (Categoria) lista.get(i);
                                                        %>
                                                        <option><% out.println(cat.getNombre());
                                                            }%></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="text-center" >
                                            <button type="submit" class="btn btn-primary" value="Modificar" name="opcion" id="opcion"><span class="glyphicon glyphicon-pencil"></span> Modificar </button>
                                            <button type="submit" class="btn btn-danger" value="Regresar" name="opcion" id="opcion"><span class="glyphicon glyphicon-remove"></span> Cancelar </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- jQuery -->
        <script src="assets/jquery-2.1.4.min.js"></script>
        <!--<script src="//code.jquery.com/jquery.js"></script>-->
        <!-- Bootstrap JavaScript -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    </body>
    <%} else {
             response.sendRedirect("index.jsp");
         }%> 
</html>
