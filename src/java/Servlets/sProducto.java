package Servlets;

import Beans.Categoria;
import Beans.Producto;
import Negocio.nCategoria;
import Negocio.nProducto;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "sProducto", urlPatterns = {"/sProducto"})
public class sProducto extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        try {
            nProducto nProd = new nProducto();
            nCategoria nCat = new nCategoria();

            if (request.getParameter("opcion").equals("Registrar")) {
                String Codigo = request.getParameter("codigo");
                String Descripcion = request.getParameter("nombre");
                String Marca = request.getParameter("marca");
                String PrecioMen = request.getParameter("preciomenor");
                Double PrecioMenor = Double.parseDouble(PrecioMen);
                String PrecioMay = request.getParameter("preciomayor");
                Double PrecioMayor = Double.parseDouble(PrecioMay);
                String PrecioCaj = request.getParameter("preciocaja");
                Double PrecioCaja = Double.parseDouble(PrecioCaj);
                String PrecioFac = request.getParameter("preciofactura");
                Double PrecioFact = Double.parseDouble(PrecioFac);
                String PrecioCos = request.getParameter("preciocosto");
                Double PrecioCost = Double.parseDouble(PrecioCos);
                String St = request.getParameter("stock");
                Integer Stock = Integer.parseInt(St);
                String Img = request.getParameter("imagen");
                String Imagen = "assets/imagenes/" + Img;
                String Categoria = request.getParameter("categoria");

                List<Categoria> cat = new ArrayList<Categoria>();
                cat = nCat.buscarCategorias(Categoria);
                int idCategoria = cat.get(0).getId();

                Producto prod = new Producto(null, Codigo, Descripcion, Marca, PrecioMenor, PrecioMayor, PrecioCaja, PrecioFact, PrecioCost ,Stock, Imagen, new Categoria(idCategoria));

                nProd.RegistrarProducto(prod);

                List<Producto> listaprod = new ArrayList<Producto>();
                listaprod = nProd.ListarProductos();
                request.setAttribute("listapro", listaprod);
                request.getRequestDispatcher("Productos.jsp").forward(request, response);
            }

            if (request.getParameter("opcion").equals("listarCategoria")) {
                List<Categoria> lista = new ArrayList<Categoria>();
                lista = nCat.listarCategorias();
                request.setAttribute("lista", lista);
            }

            if (request.getParameter("opcion").equals("listarProducto")) {
                List<Producto> listaprod = new ArrayList<Producto>();
                listaprod = nProd.ListarProductos();
                request.setAttribute("listapro", listaprod);
            }

            if (request.getParameter("opcion").equals("Eliminar")) {
                String id = request.getParameter("pro_id");
                Integer Id = Integer.parseInt(id);
                nProd.Eliminar(Id);

                List<Producto> listaprod = new ArrayList<Producto>();
                listaprod = nProd.ListarProductos();
                request.setAttribute("listapro", listaprod);
                request.getRequestDispatcher("Productos.jsp").forward(request, response);

            }

            if (request.getParameter("opcion").equals("Modificar")) {
                String id = request.getParameter("id");
                Integer idProd = Integer.parseInt(id);
                String Codigo = request.getParameter("codigo");
                String Descripcion = request.getParameter("nombre");
                String Marca = request.getParameter("marca");
                String PrecioMen = request.getParameter("preciomenor");
                Double PrecioMenor = Double.parseDouble(PrecioMen);
                String PrecioMay = request.getParameter("preciomayor");
                Double PrecioMayor = Double.parseDouble(PrecioMay);
                String PrecioCaj = request.getParameter("preciocaja");
                Double PrecioCaja = Double.parseDouble(PrecioCaj);
                String PrecioFac = request.getParameter("preciofactura");
                Double PrecioFact = Double.parseDouble(PrecioFac);
                String PrecioCos = request.getParameter("preciocosto");
                Double PrecioCost = Double.parseDouble(PrecioCos);
                String St = request.getParameter("stock");
                Integer Stock = Integer.parseInt(St);
                String Img = request.getParameter("imagen");
                String Imagen = "file:///D:/Productos/" + Img;
                String Categoria = request.getParameter("categoria");

                List<Categoria> cat = new ArrayList<Categoria>();
                cat = nCat.buscarCategorias(Categoria);
                int idCategoria = cat.get(0).getId();

                Producto prod = new Producto(idProd, Codigo, Descripcion, Marca, PrecioMenor, PrecioMayor,PrecioCaja,PrecioFact, PrecioCost,Stock, Imagen, new Categoria(idCategoria));

                nProd.ModificarProducto(prod);

                List<Producto> listaprod = new ArrayList<Producto>();
                listaprod = nProd.ListarProductos();
                request.setAttribute("listapro", listaprod);
                request.getRequestDispatcher("Productos.jsp").forward(request, response);
            }

            if (request.getParameter("opcion").equals("Regresar")) {
                request.getRequestDispatcher("Productos.jsp").forward(request, response);
            }

        } catch (Exception e) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
