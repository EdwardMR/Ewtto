/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Beans.Almacen;
import Beans.Categoria;
import Beans.Compras;
import Beans.DetalleCompra;
import Beans.Producto;
import Beans.Usuario;
import Negocio.nAlmacen;
import Negocio.nCompras;
import Negocio.nProducto;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Carlos Anthony
 */
public class sAlmacen extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            nProducto nProd = new nProducto();
            nAlmacen nAl = new nAlmacen();
            nCompras nCo=new nCompras();
            HttpSession sesion = request.getSession();
            ArrayList<Producto> carrito;
            ArrayList<Usuario> user;

            if (request.getParameter("opcion").equals("listarProducto")) {
                List<Producto> listaprod = new ArrayList<Producto>();
                listaprod = nProd.ListarProductos();
                request.setAttribute("listapro", listaprod);
            }
            if (request.getParameter("opcion").equals("Carrito")) {
                String id = request.getParameter("prod_id");
                List<Producto> listaprod = new ArrayList<Producto>();
                if (sesion.getAttribute("carrito") == null) {
                    carrito = new ArrayList<Producto>();
                } else {
                    carrito = (ArrayList<Producto>) sesion.getAttribute("carrito");
                }
                listaprod = nProd.BuscarProductoid(Integer.parseInt(id));
                Producto pro = new Producto();
                pro = (Producto) listaprod.get(0);
                int indice = -1;
                for (int i = 0; i < carrito.size(); i++) {
                    Producto pr = carrito.get(i);
                    if (pr.getCodigo().equals(pro.getCodigo())) {
                        indice = i;
                        break;
                    }
                }
                if (indice == -1) {
                    carrito.add(pro);
                } else {
                    carrito.set(indice, pro);
                }
                sesion.setAttribute("carrito", carrito);
                request.getRequestDispatcher("Almacen.jsp").forward(request, response);
            }
            if (request.getParameter("opcion").equals("RemoverCarrito")) {
                String id = request.getParameter("pro_id");
                List<Producto> listaprod = new ArrayList<Producto>();
                carrito = (ArrayList<Producto>) sesion.getAttribute("carrito");
                listaprod = nProd.BuscarProductoid(Integer.parseInt(id));
                Producto pro = new Producto();
                pro = (Producto) listaprod.get(0);
                int indice = -1;
                for (int i = 0; i < carrito.size(); i++) {
                    Producto pr = carrito.get(i);
                    if (pr.getCodigo().equals(pro.getCodigo())) {
                        indice = i;
                        break;
                    }
                }
                carrito.remove(indice);
                sesion.setAttribute("carrito", carrito);
                request.getRequestDispatcher("Almacen.jsp").forward(request, response);
            }
            if (request.getParameter("opcion").equals("Carrito1")) {
                String id = request.getParameter("prod_id");
                List<Producto> listaprod = new ArrayList<Producto>();
                if (sesion.getAttribute("carrito") == null) {
                    carrito = new ArrayList<Producto>();
                } else {
                    carrito = (ArrayList<Producto>) sesion.getAttribute("carrito");
                }
                listaprod = nProd.BuscarProductoid(Integer.parseInt(id));
                Producto pro = new Producto();
                pro = (Producto) listaprod.get(0);
                int indice = -1;
                for (int i = 0; i < carrito.size(); i++) {
                    Producto pr = carrito.get(i);
                    if (pr.getCodigo().equals(pro.getCodigo())) {
                        indice = i;
                        break;
                    }
                }
                if (indice == -1) {
                    carrito.add(pro);
                } else {
                    carrito.set(indice, pro);
                }
                sesion.setAttribute("carrito", carrito);
                request.getRequestDispatcher("ActualizarStock.jsp").forward(request, response);
            }
            if (request.getParameter("opcion").equals("RemoverCarrito1")) {
                String id = request.getParameter("pro_id");
                List<Producto> listaprod = new ArrayList<Producto>();
                carrito = (ArrayList<Producto>) sesion.getAttribute("carrito");
                listaprod = nProd.BuscarProductoid(Integer.parseInt(id));
                Producto pro = new Producto();
                pro = (Producto) listaprod.get(0);
                int indice = -1;
                for (int i = 0; i < carrito.size(); i++) {
                    Producto pr = carrito.get(i);
                    if (pr.getCodigo().equals(pro.getCodigo())) {
                        indice = i;
                        break;
                    }
                }
                carrito.remove(indice);
                sesion.setAttribute("carrito", carrito);
                request.getRequestDispatcher("ActualizarStock.jsp").forward(request, response);
            }
            if (request.getParameter("opcion").equals("Registrar")) {
                carrito = (ArrayList<Producto>) sesion.getAttribute("carrito");
                List<Almacen> Detalle = new ArrayList<Almacen>();
                List<Compras> compra = new ArrayList<Compras>();
                List<DetalleCompra> DetalleCompra = new ArrayList<DetalleCompra>();
                Compras com=new Compras(null,"F",request.getParameter("num"),null);
                nCo.RegistrarCompras(com);
                compra=nCo.UltimaCompras();
                for (int i = 0; i < carrito.size(); i++) {
                    Producto pr = carrito.get(i);
                    String a = request.getParameter("almacen" + i);
                    String b = request.getParameter("precio"+i);
                    Detalle = nAl.AlmacenxProducto(pr.getId());
                    DetalleCompra dc=new DetalleCompra(null,Integer.parseInt(a), Double.valueOf(b),pr, compra.get(0));
                    if (Detalle.size() != 0) {
                        Integer Suma = Detalle.get(0).getCantidad() + Integer.parseInt(a);
                        nAl.ModificarAlmacen(new Almacen(Detalle.get(0).getId(), Suma, null, pr));
                    } else {
                        nAl.RegistrarAlmacen(new Almacen(null, Integer.parseInt(a), null, pr));
                    }
                    nCo.RegistrarDetalleVenta(dc);
                }
                List<Usuario> lista = new ArrayList<Usuario>();

                Usuario us = new Usuario();
                user = (ArrayList<Usuario>) sesion.getAttribute("user");
                us = (Usuario) user.get(0);
                sesion.invalidate();
                HttpSession sesion1 = request.getSession();
                if (sesion1.getAttribute("user") == null) {
                    user = new ArrayList<Usuario>();
                } else {
                    user = (ArrayList<Usuario>) sesion1.getAttribute("user");
                }
                user.add(us);
                sesion1.setAttribute("user", user);
                request.getRequestDispatcher("ListaAlmacen.jsp").forward(request, response);
            }
            if (request.getParameter("opcion").equals("listarAlmacen")) {
                List<Almacen> Detalle = new ArrayList<Almacen>();
                Detalle = nAl.ListarAlmacen();
                request.setAttribute("alma", Detalle);
            }
            if (request.getParameter("opcion").equals("Actualizar")) {
                carrito = (ArrayList<Producto>) sesion.getAttribute("carrito");
                Producto p = new Producto();
                List<Almacen> Detalle = new ArrayList<Almacen>();
                for (int i = 0; i < carrito.size(); i++) {
                    p = carrito.get(i);
                    String a = request.getParameter("almacen" + i);
                    Detalle = nAl.AlmacenxProducto(p.getId());
                    Integer Suma = p.getStock() + Integer.parseInt(a);
                    Integer Resta = Detalle.get(0).getCantidad() - Integer.parseInt(a);
                    nAl.ModificarAlmacen(new Almacen(Detalle.get(0).getId(), Resta, null, p));
                    nProd.ModificarProducto(new Producto(p.getId(), p.getCodigo(), p.getDescripcion(), p.getMarca(), p.getPrecioMenor(), p.getPrecioMayor(), p.getPrecioCaja(), p.getPrecioFactura(),p.getPrecioCosto() ,Suma, p.getImagen(), new Categoria(p.getCateg().getId())));
                }
                 List<Usuario> lista = new ArrayList<Usuario>();

                Usuario us = new Usuario();
                user = (ArrayList<Usuario>) sesion.getAttribute("user");
                us = (Usuario) user.get(0);
                sesion.invalidate();
                HttpSession sesion1 = request.getSession();
                if (sesion1.getAttribute("user") == null) {
                    user = new ArrayList<Usuario>();
                } else {
                    user = (ArrayList<Usuario>) sesion1.getAttribute("user");
                }
                user.add(us);
                sesion1.setAttribute("user", user);
                request.getRequestDispatcher("Productos.jsp").forward(request, response);
            }
        } catch (Exception e) {
            out.print(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
