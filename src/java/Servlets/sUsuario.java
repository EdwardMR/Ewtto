package Servlets;

import Beans.Usuario;
import Negocio.nUsuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "sUsuario", urlPatterns = {"/sUsuario"})
public class sUsuario extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            nUsuario nUser = new nUsuario();

            if (request.getParameter("opcion").equals("Registrar")) {
                String nombre = request.getParameter("nombre");
                String Apellidop = request.getParameter("apaterno");
                String Apellidom = request.getParameter("amaterno");
                String Dni  = request.getParameter("dni");
                String Telefono = request.getParameter("telefono");
                String clave = request.getParameter("clave");
                String cuenta = request.getParameter("cuenta");
                String Tipo= "";
                
                if(request.getParameter("tipo").equals("ADMINISTRADOR")){
                    Tipo = "A";
                }
                if(request.getParameter("tipo").equals("VENDEDOR")){
                    Tipo = "V";
                }
                if(request.getParameter("tipo").equals("ALMACENERO")){
                    Tipo = "C";
                }
                
                
                Usuario us = new Usuario(null, Apellidop, Apellidom, nombre, Dni, Telefono, cuenta, clave, Tipo);
                nUser.RegistrarUsuario(us);
                request.getRequestDispatcher("Usuarios.jsp").forward(request, response);
            }
            
            if (request.getParameter("opcion").equals("ListarUsuario")) {
                List<Usuario> lista = new ArrayList<Usuario>();
                lista = nUser.ListarUsuarios();
                request.setAttribute("lista", lista);
            }
            
            if(request.getParameter("opcion").equals("Eliminar")){
                String id = request.getParameter("us_id");
                Integer Id = Integer.parseInt(id);
                nUser.Eliminar(Id);
                
                List<Usuario> lista = new ArrayList<Usuario>();
                lista = nUser.ListarUsuarios();
                request.setAttribute("lista", lista);
                request.getRequestDispatcher("Usuarios.jsp").forward(request, response);
            }
            
            if(request.getParameter("opcion").equals("Modificar")){
                String id = request.getParameter("id");
                Integer Id = Integer.parseInt(id);
                String nombre = request.getParameter("nombre");
                String Apellidop = request.getParameter("apaterno");
                String Apellidom = request.getParameter("amaterno");
                String Dni  = request.getParameter("dni");
                String Telefono = request.getParameter("telefono");
                String clave = request.getParameter("clave");
                String cuenta = request.getParameter("cuenta");
                String Tipo= "";
                
                if(request.getParameter("tipo").equals("ADMINISTRADOR")){
                    Tipo = "A";
                }
                if(request.getParameter("tipo").equals("VENDEDOR")){
                    Tipo = "V";
                }
                if(request.getParameter("tipo").equals("ALMACENERO")){
                    Tipo = "C";
                }              
                
                Usuario us = new Usuario(Id , Apellidop, Apellidom, nombre, Dni, Telefono, cuenta, clave, Tipo);
                nUser.ModificarUsuario(us);
                
                List<Usuario> lista = new ArrayList<Usuario>();
                lista = nUser.ListarUsuarios();
                request.setAttribute("lista", lista);
                request.getRequestDispatcher("Usuarios.jsp").forward(request, response);
            }
            
            if(request.getParameter("opcion").equals("Cancelar")){
                request.getRequestDispatcher("Usuarios.jsp").forward(request, response);
            }
        } catch (Exception e) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
