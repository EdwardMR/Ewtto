/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Beans.Compras;
import Beans.DetalleVenta;
import Beans.Venta;
import Negocio.nCompras;
import Negocio.nVenta;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Carlos Anthony
 */
public class sReportes extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            nVenta nVen=new nVenta();
            nCompras nCom=new nCompras();
            if (request.getParameter("opcion").equals("listaventa")) {
                List<Venta> lista = new ArrayList<Venta>();
                lista=nVen.ListarVentas();
                request.setAttribute("listapro", lista);
            }
            if (request.getParameter("opcion").equals("listacompra")) {
                List<Compras> lista = new ArrayList<Compras>();
                lista=nCom.ListarCompras();
                request.setAttribute("listapro", lista);
            }
            if (request.getParameter("opcion").equals("ventadia")) {
                List<Venta> lista = new ArrayList<Venta>();
                lista=nVen.VentasDia();
                request.setAttribute("listapro", lista);
            }
            if (request.getParameter("opcion").equals("enviar")) {
                int id=Integer.parseInt(request.getParameter("id"));
                List<DetalleVenta> lista = new ArrayList<DetalleVenta>();
                lista=nVen.BuscarDetalleVentasid(id);
                request.setAttribute("lista",lista);
                request.getRequestDispatcher("GuiaRemision.jsp").forward(request, response);
            }
            
        } catch (Exception e) {
            out.print(e);
        }
 
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
