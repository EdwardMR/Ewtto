/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Beans.Categoria;
import Negocio.nCategoria;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Carlos Anthony
 */
@WebServlet(name = "sCategorias", urlPatterns = {"/sCategorias"})
public class sCategorias extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            nCategoria cat=new nCategoria();
             if (request.getParameter("opcion").equals("Registrar")) {
                String nombre = request.getParameter("nombre");

                Categoria categ = new Categoria(null, nombre);
                cat.Insertar(categ);
                
                List<Categoria> lista = new ArrayList<Categoria>();
                lista = cat.listarCategorias();
                request.setAttribute("lista", lista);
                request.getRequestDispatcher("Categorias.jsp").forward(request, response);
            }
            if (request.getParameter("opcion").equals("Cancelar")) {
                response.sendRedirect("Categorias.jsp");
            }
            if(request.getParameter("opcion").equals("listar")){
                List<Categoria> lista = new ArrayList<Categoria>();
                lista = cat.listarCategorias();
                request.setAttribute("lista", lista);
            }
            if (request.getParameter("opcion").equals("Eliminar")) {
                String a = request.getParameter("cat_id");
                Integer Id = Integer.parseInt(a);
                cat.Eliminar(Id);
                List<Categoria> lista = new ArrayList<Categoria>();
                lista = cat.listarCategorias();
                request.setAttribute("lista", lista);
                request.getRequestDispatcher("Categorias.jsp").forward(request, response);
            }

            if (request.getParameter("opcion").equals("Modificar")) {
                String a = request.getParameter("id");
                String nomb = request.getParameter("nombre");
                Integer Id = Integer.parseInt(a);
                Categoria bcat = new Categoria(Id, nomb);
                cat.Modificar(bcat);
                List<Categoria> lista = new ArrayList<Categoria>();
                lista = cat.listarCategorias();
                request.setAttribute("lista", lista);
                request.getRequestDispatcher("Categorias.jsp").forward(request, response);
            }

            if (request.getParameter("opcion").equals("Regresar")) {
                request.getRequestDispatcher("Categorias.jsp").forward(request, response);
            }
            
            if (request.getParameter("opcion").equals("LlenarCombobox")) {
                List<Categoria> lista = new ArrayList<Categoria>();
                lista = cat.listarCategorias();
                request.setAttribute("lista", lista);
            }
            
        } catch (Exception e) {
        }
          
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
