package Negocio;

import Beans.Almacen;
import Beans.Categoria;
import Beans.Cliente;
import Beans.Compras;
import Beans.DetalleCompra;
import Beans.DetalleVenta;
import Beans.Producto;
import Beans.Remision;
import Beans.RemisionProductos;
import Beans.Usuario;
import Beans.Venta;
import ConexionBD.CADO;
import java.sql.Connection;
import java.sql.Date;
import java.util.ArrayList;;
import java.util.List;

/**
 *
 * @author Carlos Anthony
 */
public class Test {
    
    public static void main(String[] args) {
        try {
         nCompras nc=new nCompras();
         List<DetalleCompra> lista = new ArrayList<DetalleCompra>();
         lista=nc.ListarDetalleCompras();
            System.out.println(lista.get(0).getPrecio());
        } catch (Exception e) {
            System.out.println(e);
        }
        
    }
    
}
