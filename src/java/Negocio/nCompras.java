package Negocio;

import Beans.Categoria;
import Beans.Compras;
import Beans.DetalleCompra;
import Beans.Producto;
import ConexionBD.CADO;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class nCompras {
    
    public void RegistrarCompras(Compras obean) throws Exception {
        String cadena = "";
        CADO ocado = new CADO();
        try {
            cadena = "SELECT insertar_compras(?";
            for (int i = 0; i < 1; i++) {
                cadena = cadena + ",?";
            }
            cadena = cadena + ")";
            Object param[] = new Object[2];
            param[0] = obean.getDocumento().toUpperCase();
            param[1] = obean.getNumero().toUpperCase();

            ocado.EjecutaProc(cadena, param);
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }
    
    public void RegistrarDetalleVenta(DetalleCompra obean) throws Exception {
        String cadena = "";
        CADO ocado = new CADO();
        try {
            cadena = "SELECT insertar_comprasxproductos(?";
            for (int i = 0; i < 3; i++) {
                cadena = cadena + ",?";
            }
            cadena = cadena + ")";
            Object param[] = new Object[4];
            param[0] = obean.getCantidad();
            param[1] = obean.getPrecio();
            param[2] = obean.getProducto().getId();
            param[3] = obean.getCompras().getId();

            ocado.EjecutaProc(cadena, param);
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }
    
    public List<Compras> ListarCompras() throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<Compras> lista = new ArrayList<Compras>();
            lista.clear();
            cadena = "SELECT * FROM listar_tcompras()";
            CADO ocado = new CADO();
            rs = ocado.RecuperaProc(cadena);
            rs.beforeFirst();
            while (rs.next()) {
                Compras objeto = new Compras();

                objeto.setId(rs.getInt("id"));
                objeto.setFecha(rs.getDate("fecha"));
                objeto.setDocumento(rs.getString("doc"));
                objeto.setNumero(rs.getString("num"));
                
                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
    
     public List<Compras> UltimaCompras() throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<Compras> lista = new ArrayList<Compras>();
            lista.clear();
            cadena = "SELECT * FROM ultima_compra()";
            CADO ocado = new CADO();
            rs = ocado.RecuperaProc(cadena);
            rs.beforeFirst();
            while (rs.next()) {
                Compras objeto = new Compras();

                objeto.setId(rs.getInt("id"));
                objeto.setFecha(rs.getDate("fecha"));
                objeto.setDocumento(rs.getString("doc"));
                objeto.setNumero(rs.getString("num"));
                
                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
     
     public List<Compras> BuscarComprasid(Integer id) throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<Compras> lista = new ArrayList<Compras>();
            lista.clear();
            cadena = "SELECT * FROM listar_compras(?)";
            CADO ocado = new CADO();
            Object param[] = new Object[1];
            param[0] = id;
            rs = ocado.RecuperaProc(cadena, param);
            rs.beforeFirst();
            while (rs.next()) {
                Compras objeto = new Compras();

                 objeto.setId(rs.getInt("id"));
                objeto.setFecha(rs.getDate("fecha"));
                objeto.setDocumento(rs.getString("doc"));
                objeto.setNumero(rs.getString("num"));
                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public List<DetalleCompra> ListarDetalleCompras() throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<DetalleCompra> lista = new ArrayList<DetalleCompra>();
            lista.clear();
            cadena = "SELECT * FROM listar_tdetallecompra()";
            CADO ocado = new CADO();
            rs = ocado.RecuperaProc(cadena);
            rs.beforeFirst();
            while (rs.next()) {
                DetalleCompra objeto = new DetalleCompra();

                objeto.setId(rs.getInt("id"));
                objeto.setCantidad(rs.getInt("cant"));
                objeto.setPrecio(rs.getDouble("precio"));
                objeto.setProducto(new Producto(rs.getInt("productoid"), rs.getString("productocodigo"), rs.getString("productodescripcion"), rs.getString("productomarca"), rs.getDouble("productopreciomenor"), rs.getDouble("productopreciomayor"), rs.getDouble("productopreciocaja"), rs.getDouble("productopreciofactura"),rs.getDouble("productopreciocosto"), rs.getInt("productostock"), rs.getString("productoimagen"), new Categoria(rs.getInt("catid"), rs.getString("carnom"))));
                objeto.setCompras(new Compras(rs.getInt("comprasid"), rs.getString("comprasdoc"),rs.getString("comprasnum"), rs.getDate("comprasfechas")));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
    
     public List<DetalleCompra> ProductosxComprasid(Integer id) throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<DetalleCompra> lista = new ArrayList<DetalleCompra>();
            lista.clear();
            cadena = "SELECT * FROM listar_detallecompra(?)";
            CADO ocado = new CADO();
            Object param[] = new Object[1];
            param[0] = id;
            rs = ocado.RecuperaProc(cadena, param);
            rs.beforeFirst();
            while (rs.next()) {
                DetalleCompra objeto = new DetalleCompra();

                objeto.setId(rs.getInt("id"));
                objeto.setCantidad(rs.getInt("cant"));
                objeto.setPrecio(rs.getDouble("precio"));
                objeto.setProducto(new Producto(rs.getInt("productoid"), rs.getString("productocodigo"), rs.getString("productodescripcion"), rs.getString("productomarca"), rs.getDouble("productopreciomenor"), rs.getDouble("productopreciomayor"), rs.getDouble("productopreciocaja"), rs.getDouble("productopreciofactura"),rs.getDouble("productopreciocosto"), rs.getInt("productostock"), rs.getString("productoimagen"), new Categoria(rs.getInt("catid"), rs.getString("carnom"))));
                objeto.setCompras(new Compras(rs.getInt("comprasid"), rs.getString("comprasdoc"),rs.getString("comprasnum"), rs.getDate("comprasfechas")));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
}
