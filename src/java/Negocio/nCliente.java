package Negocio;

import Beans.Cliente;
import ConexionBD.CADO;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class nCliente {

    public void Insertar(Cliente obean) throws Exception {
        String cadena = "";
        CADO ocado = new CADO();
        try {
            cadena = "SELECT insertar_cliente(?";
            for (int i = 0; i < 6; i++) {
                cadena = cadena + ",?";
            }
            cadena = cadena + ")";
            Object param[] = new Object[7];
            param[0] = obean.getNombre().toUpperCase();
            param[1] = obean.getApellidoPaterno().toUpperCase();
            param[2] = obean.getApellidoMaterno().toUpperCase();
            param[3] = obean.getDireccion().toUpperCase();
            param[4] = obean.getTelefono();
            param[5] = obean.getDni();
            param[6] = obean.getRuc();
            ocado.EjecutaProc(cadena, param);
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }

    public List<Cliente> Listar() throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<Cliente> lista = new ArrayList<Cliente>();
            lista.clear();
            cadena = "SELECT * FROM listar_tcliente()";
            CADO ocado = new CADO();
            rs = ocado.RecuperaProc(cadena);
            rs.beforeFirst();
            while (rs.next()) {
                Cliente objeto = new Cliente();

                objeto.setId(rs.getInt("id"));
                objeto.setNombre(rs.getString("nombre"));
                objeto.setApellidoPaterno(rs.getString("apellidop"));
                objeto.setApellidoMaterno(rs.getString("apellidom"));
                objeto.setDireccion(rs.getString("direccion"));
                objeto.setTelefono(rs.getString("telefono"));
                objeto.setDni(rs.getString("dni"));
                objeto.setRuc(rs.getString("ruc"));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public void Modificar(Cliente obean) throws Exception {
        String Cadena = "";
        CADO ocado = new CADO();
        try {
            Cadena = "select actualizar_cliente(?";
            for (int i = 0; i < 7; i++) {
                Cadena = Cadena + ",?";
            }
            Cadena = Cadena + ")";
            Object param[] = new Object[8];
            param[0] = obean.getId();
            param[1] = obean.getNombre().toUpperCase();
            param[2] = obean.getApellidoPaterno().toUpperCase();
            param[3] = obean.getApellidoMaterno().toUpperCase();
            param[4] = obean.getDireccion().toUpperCase();
            param[5] = obean.getTelefono();
            param[6] = obean.getDni();
            param[7] = obean.getRuc();

            ocado.EjecutaProc(Cadena, param);
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }

    public void Eliminar(Integer id) throws Exception {
        String cadena = "";
        CADO ocado = new CADO();
        try {
            cadena = "SELECT eliminar_cliente(?)";
            Object param[] = new Object[1];
            param[0] = id;
            ocado.EjecutaProc(cadena, param);
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }

    public List<Cliente> Buscar(String dni) throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<Cliente> lista = new ArrayList<Cliente>();
            lista.clear();
            cadena = "SELECT * FROM listar_cliente(?)";
            CADO ocado = new CADO();
            Object param[] = new Object[1];
            param[0] = dni;
            rs = ocado.RecuperaProc(cadena, param);
            rs.beforeFirst();
            while (rs.next()) {
                Cliente objeto = new Cliente();

                objeto.setId(rs.getInt("id"));
                objeto.setNombre(rs.getString("nombre"));
                objeto.setApellidoPaterno(rs.getString("apellidop"));
                objeto.setApellidoMaterno(rs.getString("apellidom"));
                objeto.setDireccion(rs.getString("direccion"));
                objeto.setTelefono(rs.getString("telefono"));
                objeto.setDni(rs.getString("dni"));
                objeto.setRuc(rs.getString("ruc"));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
}
