package Negocio;

import Beans.Categoria;
import ConexionBD.CADO;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class nCategoria {

    public void Insertar(Categoria objeto) throws Exception {
        String cadena = "";
        CADO ocado = new CADO();
        try {
            cadena = "SELECT insertar_categoria(?)";

            Object param[] = new Object[1];
            param[0] = objeto.getNombre().toUpperCase();
            ocado.EjecutaProc(cadena, param);
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }

    public List<Categoria> listarCategorias() throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<Categoria> lista = new ArrayList<Categoria>();
            lista.clear();
            cadena = "SELECT * FROM listar_tcategoria()";
            CADO ocado = new CADO();
            rs = ocado.RecuperaProc(cadena);
            rs.beforeFirst();

            while (rs.next()) {
                Categoria objeto = new Categoria();

                objeto.setId(rs.getInt("id"));
                objeto.setNombre(rs.getString("nombre"));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Categoria> buscarCategorias(String nombre) throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<Categoria> lista = new ArrayList<Categoria>();
            lista.clear();
            cadena = "SELECT * FROM listar_categoria(?)";
            CADO ocado = new CADO();
            Object param[] = new Object[1];
            param[0] = nombre;
            rs = ocado.RecuperaProc(cadena, param);
            rs.beforeFirst();
            while (rs.next()) {
                Categoria objeto = new Categoria();

                objeto.setId(rs.getInt("id"));
                objeto.setNombre(rs.getString("nombre"));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public void Eliminar(Integer id) throws Exception {
        String cadena = "";
        CADO ocado = new CADO();
        try {
            cadena = "SELECT eliminar_categoria(?)";
            Object param[] = new Object[1];
            param[0] = id;
            ocado.EjecutaProc(cadena, param);
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }

    public void Modificar(Categoria objeto) throws Exception {
        String Cadena = "";
        CADO ocado = new CADO();
        try {
            Cadena = "select actualizar_categoria(?,?)";
            Object param[] = new Object[2];
            param[0] = objeto.getId();
            param[1] = objeto.getNombre().toUpperCase();

            ocado.EjecutaProc(Cadena, param);
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }
}
