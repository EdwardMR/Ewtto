package Negocio;

import Beans.Categoria;
import Beans.Producto;
import ConexionBD.CADO;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class nProducto {

    public void RegistrarProducto(Producto obean) throws Exception {
        String cadena = "";
        CADO ocado = new CADO();
        try {
            cadena = "SELECT insertar_producto(?";
            for (int i = 0; i < 10; i++) {
                cadena = cadena + ",?";
            }
            cadena = cadena + ")";
            Object param[] = new Object[11];
            param[0] = obean.getCodigo();
            param[1] = obean.getDescripcion().toUpperCase();
            param[2] = obean.getMarca().toUpperCase();
            param[3] = obean.getPrecioMenor();
            param[4] = obean.getPrecioMayor();
            param[5] = obean.getPrecioCaja();
            param[6] = obean.getPrecioFactura();
            param[7] = obean.getStock();
            param[8] = obean.getImagen();
            param[9] = obean.getCateg().getId();
            param[10] = obean.getPrecioCosto();

            ocado.EjecutaProc(cadena, param);
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }

    public List<Producto> ListarProductos() throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<Producto> lista = new ArrayList<Producto>();
            lista.clear();
            cadena = "SELECT * FROM listar_tproducto()";
            CADO ocado = new CADO();
            rs = ocado.RecuperaProc(cadena);
            rs.beforeFirst();
            while (rs.next()) {
                Producto objeto = new Producto();

                objeto.setId(rs.getInt("id"));
                objeto.setCodigo(rs.getString("codigo"));
                objeto.setDescripcion(rs.getString("descripcion"));
                objeto.setMarca(rs.getString("marca"));
                objeto.setPrecioMenor(rs.getDouble("preciomenor"));
                objeto.setPrecioMayor(rs.getDouble("preciomayor"));
                objeto.setPrecioCaja(rs.getDouble("preciocaja"));
                objeto.setPrecioFactura(rs.getDouble("preciofactura"));
                objeto.setPrecioCosto(rs.getDouble("preciocosto"));
                objeto.setStock(rs.getInt("stock"));
                objeto.setImagen(rs.getString("imagen"));
                objeto.setCateg(new Categoria(rs.getInt("catid"), rs.getString("carnom")));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Producto> BuscarProductoid(Integer id) throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<Producto> lista = new ArrayList<Producto>();
            lista.clear();
            cadena = "SELECT * FROM listar_producto(?)";
            CADO ocado = new CADO();
            Object param[] = new Object[1];
            param[0] = id;
            rs = ocado.RecuperaProc(cadena, param);
            rs.beforeFirst();
            while (rs.next()) {
                Producto objeto = new Producto();

                objeto.setId(rs.getInt("id"));
                objeto.setCodigo(rs.getString("codigo"));
                objeto.setDescripcion(rs.getString("descripcion"));
                objeto.setMarca(rs.getString("marca"));
                objeto.setPrecioMenor(rs.getDouble("preciomenor"));
                objeto.setPrecioMayor(rs.getDouble("preciomayor"));
                objeto.setPrecioCaja(rs.getDouble("preciocaja"));
                objeto.setPrecioFactura(rs.getDouble("preciofactura"));
                objeto.setPrecioCosto(rs.getDouble("preciocosto"));
                objeto.setStock(rs.getInt("stock"));
                objeto.setImagen(rs.getString("imagen"));
                objeto.setCateg(new Categoria(rs.getInt("catid"), rs.getString("carnom")));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public void Eliminar(Integer id) throws Exception {
        String cadena = "";
        CADO ocado = new CADO();
        try {
            
            cadena = "SELECT eliminar_producto(?)";
            Object param[] = new Object[1];
            param[0] = id;
            ocado.EjecutaProc(cadena, param);
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }

    public void ModificarProducto(Producto obean) throws Exception {
        String Cadena = "";
        CADO ocado = new CADO();
        try {
            
            Cadena = "select actualizar_producto(?";
            for (int i = 0; i < 11; i++) {
                Cadena = Cadena + ",?";
            }
            Cadena = Cadena + ")";
            Object param[] = new Object[12];
            param[0] = obean.getId();
            param[1] = obean.getCodigo().toUpperCase();
            param[2] = obean.getDescripcion().toUpperCase();
            param[3] = obean.getMarca().toUpperCase();
            param[4] = obean.getPrecioMenor();
            param[5] = obean.getPrecioMayor();
            param[6] = obean.getPrecioCaja();
            param[7] = obean.getPrecioFactura();
            param[8] = obean.getStock();
            param[9] = obean.getImagen();
            param[10] = obean.getCateg().getId();
            param[11] = obean.getPrecioCosto();

            ocado.EjecutaProc(Cadena, param);
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }
}
