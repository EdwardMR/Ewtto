package Negocio;

import Beans.Categoria;
import Beans.Cliente;
import Beans.Producto;
import Beans.Remision;
import Beans.RemisionProductos;
import Beans.Usuario;
import Beans.Venta;
import ConexionBD.CADO;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class nRemision {

    public void RegistrarRemision(Remision obean) throws Exception {
        String cadena = "";
        CADO ocado = new CADO();
        try {
            cadena = "SELECT insertar_remision(?";
            for (int i = 0; i < 5; i++) {
                cadena = cadena + ",?";
            }
            cadena = cadena + ")";
            Object param[] = new Object[6];
            param[0] = obean.getFechaFin();
            param[1] = obean.getPartida().toUpperCase();
            param[2] = obean.getLlegada().toUpperCase();
            param[3] = obean.getRazon();
            param[4] = obean.getVenta().getId();
            param[5] = obean.getCod();

            ocado.EjecutaProc(cadena, param);
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }

    public void RegistrarRemisionxProducto(RemisionProductos obean) throws Exception {
        String cadena = "";
        CADO ocado = new CADO();
        try {
            cadena = "SELECT insertar_remisionxproductos(?";
            for (int i = 0; i < 3; i++) {
                cadena = cadena + ",?";
            }
            cadena = cadena + ")";
            Object param[] = new Object[4];
            param[0] = obean.getCantidad();
            param[1] = obean.getPrecio();
            param[2] = obean.getProductos().getId();
            param[3] = obean.getRemision().getId();

            ocado.EjecutaProc(cadena, param);
        } catch (Exception e) {
            throw e;
        }finally{
            ocado.CerrarSeccion();
        }
    }

    public List<Remision> ListarRemision() throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<Remision> lista = new ArrayList<Remision>();
            lista.clear();
            cadena = "SELECT * FROM listar_tremision()";
            CADO ocado = new CADO();
            rs = ocado.RecuperaProc(cadena);
            rs.beforeFirst();
            while (rs.next()) {
                Remision objeto = new Remision();

                objeto.setId(rs.getInt("id"));
                objeto.setCod(rs.getString("cod"));
                objeto.setFechaInicio(rs.getDate("fechainicio"));
                objeto.setFechaFin(rs.getDate("fechafin"));
                objeto.setPartida(rs.getString("partida"));
                objeto.setLlegada(rs.getString("llegada"));
                objeto.setRazon(rs.getString("razon"));
                objeto.setVenta(new Venta(rs.getInt("ventasid"), rs.getDate("ventasfecha"), rs.getDouble("ventastotal"), rs.getString("ventasdoc"), rs.getString("ventasnum"), new Cliente(rs.getInt("clienteid"), rs.getString("clientenombre"), rs.getString("clienteapellidop"), rs.getString("clienteapellidom"), rs.getString("clientedireccion"), rs.getString("clientetelefono"), rs.getString("clientedni"), rs.getString("clienteruc")), new Usuario(rs.getInt("usuarioid"), rs.getString("usuarioapellidop"), rs.getString("usuarioapellidom"), rs.getString("usuarionombre"), rs.getString("usuariodni"), rs.getString("usuariotelefono"), rs.getString("usuariocuenta"), rs.getString("usuarioclave"), rs.getString("usuariotipo"))));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<RemisionProductos> ListarRemisionProducto() throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<RemisionProductos> lista = new ArrayList<RemisionProductos>();
            lista.clear();
            cadena = "SELECT * FROM listar_tremisionxproductos()";
            CADO ocado = new CADO();
            rs = ocado.RecuperaProc(cadena);
            rs.beforeFirst();
            while (rs.next()) {
                RemisionProductos objeto = new RemisionProductos();

                objeto.setId(rs.getInt("id"));
                objeto.setCantidad(rs.getInt("cantidad"));
                objeto.setPrecio(rs.getDouble("precio"));
                objeto.setProductos(new Producto(rs.getInt("productoid"), rs.getString("productocodigo"), rs.getString("productodescripcion"), rs.getString("productomarca"), rs.getDouble("productopreciomenor"), rs.getDouble("productopreciomayor"), rs.getDouble("productopreciocaja"), rs.getDouble("productopreciofactura"),rs.getDouble("productopreciocosto"), rs.getInt("productostock"), rs.getString("productoimagen"), new Categoria(rs.getInt("catid"), rs.getString("carnom"))));
                objeto.setRemision(new Remision(rs.getInt("remisionid"), rs.getString("remisioncod"), rs.getDate("remisionfechainicio"), rs.getDate("remisionfechafin"), rs.getString("remisionpartida"), rs.getString("remisionllegada"), rs.getString("remisionrazon"), new Venta(rs.getInt("ventasid"), rs.getDate("ventasfecha"), rs.getDouble("ventastotal"), rs.getString("ventasdoc"), rs.getString("ventasnum"), new Cliente(rs.getInt("clienteid"), rs.getString("clientenombre"), rs.getString("clienteapellidop"), rs.getString("clienteapellidom"), rs.getString("clientedireccion"), rs.getString("clientetelefono"), rs.getString("clientedni"), rs.getString("clienteruc")), new Usuario(rs.getInt("usuarioid"), rs.getString("usuarioapellidop"), rs.getString("usuarioapellidom"), rs.getString("usuarionombre"), rs.getString("usuariodni"), rs.getString("usuariotelefono"), rs.getString("usuariocuenta"), rs.getString("usuarioclave"), rs.getString("usuariotipo")))));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Remision> BuscarRemision(Integer id) throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<Remision> lista = new ArrayList<Remision>();
            lista.clear();
            cadena = "SELECT * FROM listar_remision(?)";
            CADO ocado = new CADO();
            Object param[] = new Object[1];
            param[0] = id;
            rs = ocado.RecuperaProc(cadena, param);
            rs.beforeFirst();
            while (rs.next()) {
                Remision objeto = new Remision();

                objeto.setId(rs.getInt("id"));
                objeto.setCod(rs.getString("cod"));
                objeto.setFechaInicio(rs.getDate("fechainicio"));
                objeto.setFechaFin(rs.getDate("fechafin"));
                objeto.setPartida(rs.getString("partida"));
                objeto.setLlegada(rs.getString("llegada"));
                objeto.setRazon(rs.getString("razon"));
                objeto.setVenta(new Venta(rs.getInt("ventasid"), rs.getDate("ventasfecha"), rs.getDouble("ventastotal"), rs.getString("ventasdoc"), rs.getString("ventasnum"), new Cliente(rs.getInt("clienteid"), rs.getString("clientenombre"), rs.getString("clienteapellidop"), rs.getString("clienteapellidom"), rs.getString("clientedireccion"), rs.getString("clientetelefono"), rs.getString("clientedni"), rs.getString("clienteruc")), new Usuario(rs.getInt("usuarioid"), rs.getString("usuarioapellidop"), rs.getString("usuarioapellidom"), rs.getString("usuarionombre"), rs.getString("usuariodni"), rs.getString("usuariotelefono"), rs.getString("usuariocuenta"), rs.getString("usuarioclave"), rs.getString("usuariotipo"))));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<RemisionProductos> BuscarRemisionProductos(Integer id) throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<RemisionProductos> lista = new ArrayList<RemisionProductos>();
            lista.clear();
            cadena = "SELECT * FROM listar_remisionxproductos(?)";
            CADO ocado = new CADO();
            Object param[] = new Object[1];
            param[0] = id;
            rs = ocado.RecuperaProc(cadena, param);
            rs.beforeFirst();
            while (rs.next()) {
                RemisionProductos objeto = new RemisionProductos();

                objeto.setId(rs.getInt("id"));
                objeto.setCantidad(rs.getInt("cantidad"));
                objeto.setPrecio(rs.getDouble("precio"));
                objeto.setProductos(new Producto(rs.getInt("productoid"), rs.getString("productocodigo"), rs.getString("productodescripcion"), rs.getString("productomarca"), rs.getDouble("productopreciomenor"), rs.getDouble("productopreciomayor"), rs.getDouble("productopreciocaja"), rs.getDouble("productopreciofactura"),rs.getDouble("productopreciocosto"), rs.getInt("productostock"), rs.getString("productoimagen"), new Categoria(rs.getInt("catid"), rs.getString("carnom"))));
                objeto.setRemision(new Remision(rs.getInt("remisionid"), rs.getString("remisioncod"), rs.getDate("remisionfechainicio"), rs.getDate("remisionfechafin"), rs.getString("remisionpartida"), rs.getString("remisionllegada"), rs.getString("remisionrazon"), new Venta(rs.getInt("ventasid"), rs.getDate("ventasfecha"), rs.getDouble("ventastotal"), rs.getString("ventasdoc"), rs.getString("ventasnum"), new Cliente(rs.getInt("clienteid"), rs.getString("clientenombre"), rs.getString("clienteapellidop"), rs.getString("clienteapellidom"), rs.getString("clientedireccion"), rs.getString("clientetelefono"), rs.getString("clientedni"), rs.getString("clienteruc")), new Usuario(rs.getInt("usuarioid"), rs.getString("usuarioapellidop"), rs.getString("usuarioapellidom"), rs.getString("usuarionombre"), rs.getString("usuariodni"), rs.getString("usuariotelefono"), rs.getString("usuariocuenta"), rs.getString("usuarioclave"), rs.getString("usuariotipo")))));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Remision> UltimaRemision() throws Exception {
        String cadena = "";
        ResultSet rs;
        try {
            List<Remision> lista = new ArrayList<Remision>();
            lista.clear();
            cadena = "SELECT * FROM ultima_remision()";
            CADO ocado = new CADO();
            rs = ocado.RecuperaProc(cadena);
            rs.beforeFirst();
            while (rs.next()) {
                Remision objeto = new Remision();

                objeto.setId(rs.getInt("id"));
                objeto.setCod(rs.getString("cod"));
                objeto.setFechaInicio(rs.getDate("fechainicio"));
                objeto.setFechaFin(rs.getDate("fechafin"));
                objeto.setPartida(rs.getString("partida"));
                objeto.setLlegada(rs.getString("llegada"));
                objeto.setRazon(rs.getString("razon"));
                objeto.setVenta(new Venta(rs.getInt("ventasid"), rs.getDate("ventasfecha"), rs.getDouble("ventastotal"), rs.getString("ventasdoc"), rs.getString("ventasnum"), new Cliente(rs.getInt("clienteid"), rs.getString("clientenombre"), rs.getString("clienteapellidop"), rs.getString("clienteapellidom"), rs.getString("clientedireccion"), rs.getString("clientetelefono"), rs.getString("clientedni"), rs.getString("clienteruc")), new Usuario(rs.getInt("usuarioid"), rs.getString("usuarioapellidop"), rs.getString("usuarioapellidom"), rs.getString("usuarionombre"), rs.getString("usuariodni"), rs.getString("usuariotelefono"), rs.getString("usuariocuenta"), rs.getString("usuarioclave"), rs.getString("usuariotipo"))));

                lista.add(objeto);
            }
            rs.close();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
}
