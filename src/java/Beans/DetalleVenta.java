package Beans;

public class DetalleVenta {
    Integer id;
    Integer Cantidad;
    Double Precio;
    Producto Producto;
    Venta Venta;

    public DetalleVenta() {
    }

    public DetalleVenta(Integer id) {
        this.id = id;
    }

    public DetalleVenta(Integer id, Integer Cantidad, Double Precio, Producto Producto, Venta Venta) {
        this.id = id;
        this.Cantidad = Cantidad;
        this.Precio = Precio;
        this.Producto = Producto;
        this.Venta = Venta;
    }

    public Integer getCantidad() {
        return Cantidad;
    }

    public Double getPrecio() {
        return Precio;
    }

    public void setPrecio(Double Precio) {
        this.Precio = Precio;
    }

    public void setCantidad(Integer Cantidad) {
        this.Cantidad = Cantidad;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Producto getProducto() {
        return Producto;
    }

    public void setProducto(Producto Producto) {
        this.Producto = Producto;
    }

    public Venta getVenta() {
        return Venta;
    }

    public void setVenta(Venta Venta) {
        this.Venta = Venta;
    }        
}
