package Beans;

import java.util.Date;

public class Almacen {
    
    Integer Id;
    Integer Cantidad;
    Date Fecha;
    Producto Producto;

    public Almacen() {
    }

    public Almacen(Integer Id) {
        this.Id = Id;
    }

    public Almacen(Integer Id, Integer Cantidad, Date Fecha, Producto Producto) {
        this.Id = Id;
        this.Cantidad = Cantidad;
        this.Fecha = Fecha;
        this.Producto = Producto;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public Integer getCantidad() {
        return Cantidad;
    }

    public void setCantidad(Integer Cantidad) {
        this.Cantidad = Cantidad;
    }

    public Date getFecha() {
        return Fecha;
    }

    public void setFecha(Date Fecha) {
        this.Fecha = Fecha;
    }

    public Producto getProducto() {
        return Producto;
    }

    public void setProducto(Producto Producto) {
        this.Producto = Producto;
    }
    
    
    
}
