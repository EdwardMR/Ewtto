package Beans;

public class Categoria {
    private Integer Id;
    private String Nombre;

    public Categoria() {
    }

    public Categoria(Integer Id) {
        this.Id = Id;
    }

    public Categoria(Integer Id, String Nombre) {
        this.Id = Id;
        this.Nombre = Nombre;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    @Override
    public String toString() {
        return this.Nombre;
    }
    
    
    
}
