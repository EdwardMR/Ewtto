package Beans;

public class Producto {

    private Integer Id;
    private String Codigo;
    private String Descripcion;
    private String Marca;
    private Double PrecioMenor;
    private Double PrecioMayor;
    private Double PrecioCaja;
    private Double PrecioFactura;
    private Double PrecioCosto;
    private Integer Stock;
    private String Imagen;
    private Categoria categ;

    public Producto() {
    }

    public Producto(Integer Id) {
        this.Id = Id;
    }

    public Producto(Integer Id, String Codigo, String Descripcion, String Marca, Double PrecioMenor, Double PrecioMayor, Double PrecioCaja, Double PrecioFactura, Double PrecioCosto, Integer Stock, String Imagen, Categoria categ) {
        this.Id = Id;
        this.Codigo = Codigo;
        this.Descripcion = Descripcion;
        this.Marca = Marca;
        this.PrecioMenor = PrecioMenor;
        this.PrecioMayor = PrecioMayor;
        this.PrecioCaja = PrecioCaja;
        this.PrecioFactura = PrecioFactura;
        this.PrecioCosto = PrecioCosto;
        this.Stock = Stock;
        this.Imagen = Imagen;
        this.categ = categ;
    }


    public Double getPrecioCaja() {
        return PrecioCaja;
    }

    public void setPrecioCaja(Double PrecioCaja) {
        this.PrecioCaja = PrecioCaja;
    }

    public Double getPrecioFactura() {
        return PrecioFactura;
    }

    public void setPrecioFactura(Double PrecioFactura) {
        this.PrecioFactura = PrecioFactura;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    public Double getPrecioMenor() {
        return PrecioMenor;
    }

    public void setPrecioMenor(Double PrecioMenor) {
        this.PrecioMenor = PrecioMenor;
    }

    public Double getPrecioMayor() {
        return PrecioMayor;
    }

    public void setPrecioMayor(Double PrecioMayor) {
        this.PrecioMayor = PrecioMayor;
    }

    public Double getPrecioCosto() {
        return PrecioCosto;
    }

    public void setPrecioCosto(Double PrecioCosto) {
        this.PrecioCosto = PrecioCosto;
    }

    public Integer getStock() {
        return Stock;
    }

    public void setStock(Integer Stock) {
        this.Stock = Stock;
    }

    public String getImagen() {
        return Imagen;
    }

    public void setImagen(String Imagen) {
        this.Imagen = Imagen;
    }

    public Categoria getCateg() {
        return categ;
    }

    public void setCateg(Categoria categ) {
        this.categ = categ;
    }

    @Override
    public String toString() {
        return this.Descripcion;
    }

}
