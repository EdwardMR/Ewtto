package Beans;

import java.util.Date;

public class Remision {
    Integer Id;
    String Cod;
    Date FechaInicio;
    Date FechaFin;
    String Partida;
    String Llegada;
    String Razon;
    Venta Venta;

    public Remision() {
    }

    public Remision(Integer Id) {
        this.Id = Id;
    }

    public Remision(Integer Id, String Cod, Date FechaInicio, Date FechaFin, String Partida, String Llegada, String Razon, Venta Venta) {
        this.Id = Id;
        this.Cod = Cod;
        this.FechaInicio = FechaInicio;
        this.FechaFin = FechaFin;
        this.Partida = Partida;
        this.Llegada = Llegada;
        this.Razon = Razon;
        this.Venta = Venta;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getCod() {
        return Cod;
    }

    public void setCod(String Cod) {
        this.Cod = Cod;
    }

    public Date getFechaInicio() {
        return FechaInicio;
    }

    public void setFechaInicio(Date FechaInicio) {
        this.FechaInicio = FechaInicio;
    }

    public Date getFechaFin() {
        return FechaFin;
    }

    public void setFechaFin(Date FechaFin) {
        this.FechaFin = FechaFin;
    }

    public String getPartida() {
        return Partida;
    }

    public void setPartida(String Partida) {
        this.Partida = Partida;
    }

    public String getLlegada() {
        return Llegada;
    }

    public void setLlegada(String Llegada) {
        this.Llegada = Llegada;
    }

    public String getRazon() {
        return Razon;
    }

    public void setRazon(String Razon) {
        this.Razon = Razon;
    }

    public Venta getVenta() {
        return Venta;
    }

    public void setVenta(Venta Venta) {
        this.Venta = Venta;
    }
  
}
