package Beans;

import java.util.logging.Logger;

public class RemisionProductos {
    
    Integer Id;
    Integer Cantidad;
    Double Precio;
    Producto Productos;
    Remision Remision;

    public RemisionProductos() {
    }

    public RemisionProductos(Integer Id) {
        this.Id = Id;
    }

    public RemisionProductos(Integer Id, Integer Cantidad, Double Precio, Producto Productos, Remision Remision) {
        this.Id = Id;
        this.Cantidad = Cantidad;
        this.Precio = Precio;
        this.Productos = Productos;
        this.Remision = Remision;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public Integer getCantidad() {
        return Cantidad;
    }

    public void setCantidad(Integer Cantidad) {
        this.Cantidad = Cantidad;
    }

    public Double getPrecio() {
        return Precio;
    }

    public void setPrecio(Double Precio) {
        this.Precio = Precio;
    }

    public Producto getProductos() {
        return Productos;
    }

    public void setProductos(Producto Productos) {
        this.Productos = Productos;
    }

    public Remision getRemision() {
        return Remision;
    }

    public void setRemision(Remision Remision) {
        this.Remision = Remision;
    }
    
    
    
}
