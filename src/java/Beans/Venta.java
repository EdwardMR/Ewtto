package Beans;

import java.util.Date;

public class Venta {

    Integer id;
    Date Fecha;
    Double Total;
    String Documento;
    String Numero;
    Cliente Cliente;
    Usuario Usuario;

    public Venta() {
    }

    public Venta(Integer id) {
        this.id = id;
    }

    public Venta(Integer id, Date Fecha, Double Total, String Documento, String Numero, Cliente Cliente, Usuario Usuario) {
        this.id = id;
        this.Fecha = Fecha;
        this.Total = Total;
        this.Documento = Documento;
        this.Numero = Numero;
        this.Cliente = Cliente;
        this.Usuario = Usuario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecha() {
        return Fecha;
    }

    public void setFecha(Date Fecha) {
        this.Fecha = Fecha;
    }

    public Double getTotal() {
        return Total;
    }

    public void setTotal(Double Total) {
        this.Total = Total;
    }
    
    public String getDocumento() {
        return Documento;
    }

    public void setDocumento(String Documento) {
        this.Documento = Documento;
    }

    public String getNumero() {
        return Numero;
    }

    public void setNumero(String Numero) {
        this.Numero = Numero;
    }

    public Cliente getCliente() {
        return Cliente;
    }

    public void setCliente(Cliente Cliente) {
        this.Cliente = Cliente;
    }

    public Usuario getUsuario() {
        return Usuario;
    }

    public void setUsuario(Usuario Usuario) {
        this.Usuario = Usuario;
    }
}
